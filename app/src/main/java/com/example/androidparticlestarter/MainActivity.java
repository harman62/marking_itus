package com.example.androidparticlestarter;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    // MARK: Debug info
    private final String TAG = "";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "harmankaur.id@gmail.com";
    private final String PARTICLE_PASSWORD = "Zxcv@1234";

    Button btnMonitoring;
    TextView txtTimeElapsed;
    TextView txtTimeLeft;

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "42002a000247363333343435";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMonitoring = findViewById(R.id.btnMonitoring);
        txtTimeElapsed = findViewById(R.id.txtTimeElapsed);
        txtTimeLeft = findViewById(R.id.txtTimeLeft);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

    }



    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }




    public void startMonitoring(View view) {
        Toast.makeText(getApplicationContext(), "Start Monitoring Pressed", Toast.LENGTH_SHORT)
                .show();

        startTimer();


    }

    public void makeHappyFace(Integer amount) {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------

                // what functions are "public" on the particle?
                Log.d(TAG, "Available functions: " + mDevice.getFunctions());


                List<String> functionParameters = new ArrayList<String>();
                //functionParameters.add();
                try {
                    mDevice.callFunction("smile"+amount, functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });


    }

    long startTime = 20000;
    int timeElapsed;
    Boolean ButtonClicked = false;

    public void startTimer() {

        new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                txtTimeLeft.setText("seconds remaining: " + millisUntilFinished / 1000);

                timeElapsed = Math.toIntExact(millisUntilFinished/1000);
                Log.d("time", String.valueOf(timeElapsed));

                if(timeElapsed == 18){
                    makeHappyFace(0);
                }
                if(timeElapsed == 15){
                    makeHappyFace(1);
                }
                if(timeElapsed == 10){
                    makeHappyFace(2);
                }
                if(timeElapsed == 5){
                    makeHappyFace(3);
                }
                if(timeElapsed == 2){
                    makeHappyFace(4);
                }
//                int i = 1;
//                for(int j = timeElapsed; j >= 0 ; j = j-4) {
//                    while(i<5) {
//                        makeHappyFace(i);
//                        i++;
//                    }
//                }

            }

            public void onFinish() {
                txtTimeLeft.setText("done!");
            }
        }.start();
//
    }
}
